--localization file for english/United States
local AceLocale = LibStub:GetLibrary("AceLocale-3.0")
local L = AceLocale:NewLocale("FeignDeathResistAlert", "enUS", true)

L["FeignDeathResistAlert"] = true
L["Profiles"] = true
L["Options"] = true

L["Introduction"] = true

L["FlashScreen"] = "Flash screen"
L["FlashScreenDescription"] = "Flash the screen with a visual alert"

L["Sound"] = "Play sound"
L["SoundDescription"] = "Play an audible alert sound"

L["AnnounceSay"]= "Announce with /say"
L["AnnounceSayDescription"] = "Announce with /say that your Feign Death was resisted"

L["AnnounceGroup"]= "Announce in party or raid"
L["AnnounceGroupDescription"] = "Announce in party chat or raid chat that your Feign Death was resisted"

L["MessageFD"] = "Announcement message for Feign Death resist"
L["MessageEW"] = "Announcement message for Exposed Weakness"
L["MessageDescription"] = "The message that will be announced in /say and/or party or raid chat"

L["MessageHelp"] = [[|cffffd000$p|cffffffff will be replaced with your name.
|cffffd000$t|cffffffff will be replaced with your target's name.]]

L["MessageDefaultFD"] = "My Feign Death was resisted on $t"
L["MessageDefaultEW"] = "Exposed Weakness on $t"
L["MessageReset"] = "Default Message"

L["Scourge Horn"] = true
L["Grunt Horn"] = true
L["War Drums"] = true

-- Support section
L["SupportText"] = [[
NOTE: The game reports Feign Death resists only if you are targeting the NPC who resists your Feign Death.

For feature requests and bugs, visit:

    |cffffd000sparda972@gmail.com|cffffffff
]]
