# FeignDeathResistAlert

A World of Warcraft Classic addon that provides various visual and audible alerts when your Hunter's Feign Death is resisted by your target. There have been similar addons created over the history of WoW but this one just adds some more options and a bit more configurable.

This addon has only been tested on the Classic 1.13.x client. Untested in retail/BFA/etc but may work fine.

## Manual Installation

Install the addon into the addons folder. The folder structure should look similar to this:

```
Interface/
    AddOns/
        FeignDeathResistAlert/
            FeignDeathResistAlert.toc
            core.lua
            options.lua
            embeds.xml
            Locales/
            Libs/
            ...
```

## Options

All options are available in the Blizzard Interface Options window. You can also open the options by type `/feigndeathresistalert`

* Play sound
    * none
    * Scourge Horn
    * Grunt Horn
    * War Drums
* Flash screen
    * Flash a visual indicator on the screen
* Announce in /say
    * Put a customizable message in `/say`
* Announce in party or raid
    * Put a customizable message in party or raid chat
* Announcement message
    * Customize the message that is announced
    * `$p` will be replaced with your character's name
    * `$t` will be replaced with your target's name
* Default Message
    * Resets the announcement message to it's default value

## Notes

After some experimentation, it appears that World of Warcraft Classic will only report Feign Death resists if you are targeting the NPC that resists your Feign Death when it happens. If you are not targeting the NPC this addon will not provide any alerts. If you find information contrary to this, open a ticket.
