## Interface 13300
## Title: FeignDeathResistAlert
## Notes: Visual alerts when a Hunter's Feign Death ability is resisted
## Author: Jakobud
## Version: 1.0
## DefaultState: enabled
## SavedVariables: FeignDeathResistAlertDB

embeds.xml
core.lua
options.lua
