local _, FeignDeathResistAlert = ...

-- Localization
local L = LibStub("AceLocale-3.0"):GetLocale("FeignDeathResistAlert")

FeignDeathResistAlert.options = {
    name = L["FeignDeathResistAlert"],
    type = "group",
    order = 1,
    args = {
        Options = {
            name = L["Options"],
            type = "group",
            order = 1,
            args = {
--                flashScreen = {
--                    name = L["FlashScreen"],
--                    desc = L["FlashScreenDescription"],
--                    type = "toggle",
--                    order = 2,
--                    width = "double",
--                    get = function()
--                        return FeignDeathResistAlert.db.profile.flashScreen
--                    end,
--                    set = function(info, value)
--                        FeignDeathResistAlert.db.profile.flashScreen = value
--                    end,
--                },
                sound = {
                    name = L["Sound"],
                    desc = L["SoundDescription"],
                    type = "select",
                    width = "double",
                    order = 1,
                    values = {
                        ["none"] = "None",
                        ["scourge_horn"] = L["Scourge Horn"],
                        ["grunt_horn"] = L["Grunt Horn"],
                        ["war_drums"] = L["War Drums"],
                    },
                    sorting = {
                        "none",
                        "scourge_horn",
                        "grunt_horn",
                        "war_drums",
                    },
                    get = function(info)
                        return FeignDeathResistAlert.db.profile.sound
                    end,
                    set = function(info, value)
                        FeignDeathResistAlert.db.profile.sound = value
                        PlaySoundFile(FeignDeathResistAlert.sounds[value], 'Master')
                    end,
                },
--                announceSay = {
--                    name = L["AnnounceSay"],
--                    desc = L["AnnounceSayDescription"],
--                    type = "toggle",
--                    order = 4,
--                    width = "double",
--                    get = function()
--                        return FeignDeathResistAlert.db.profile.announceSay
--                    end,
--                    set = function(info, value)
--                        FeignDeathResistAlert.db.profile.announceSay = value
--                    end,
--                },
--                announceGroup = {
--                    name = L["AnnounceGroup"],
--                    desc = L["AnnounceGroupDescription"],
--                    type = "toggle",
--                    order = 5,
--                    width = "double",
--                    get = function()
--                        return FeignDeathResistAlert.db.profile.announceGroup
--                    end,
--                    set = function(info, value)
--                        FeignDeathResistAlert.db.profile.announceGroup = value
--                    end,
--                },
                messageFD = {
                    name = L["MessageFD"],
                    desc = L["MessageDescription"],
                    type = "input",
                    order = 6,
                    width = "full",
                    get = function(info)
                        return FeignDeathResistAlert.db.profile.messageFD
                    end,
                    set = function(info, value)
                        FeignDeathResistAlert.db.profile.messageFD = value
                    end,
                    hidden = function(info)
                        return (FeignDeathResistAlert.db.profile.announceSay == false and FeignDeathResistAlert.db.profile.announceGroup == false)
                    end
                },
                messageEW = {
                    name = L["MessageEW"],
                    desc = L["MessageDescription"],
                    type = "input",
                    order = 6,
                    width = "full",
                    get = function(info)
                        return FeignDeathResistAlert.db.profile.messageEW
                    end,
                    set = function(info, value)
                        FeignDeathResistAlert.db.profile.messageEW = value
                    end,
                    hidden = function(info)
                        return (FeignDeathResistAlert.db.profile.announceSay == false and FeignDeathResistAlert.db.profile.announceGroup == false)
                    end
                },
                messageHelp = {
                    name = L["MessageHelp"],
                    type = "description",
                    order = 9,
                },
                messageReset = {
                    name = L["MessageReset"],
                    type = "execute",
                    order = 10,
                    func = function()
                        FeignDeathResistAlert.db.profile.messageFD = L["MessageDefaultFD"]
                        FeignDeathResistAlert.db.profile.messageEW = L["MessageDefaultEW"]
                    end
                },
                supportGroup = {
                    name = " ",
                    type = "group",
                    order = 12,
                    inline = true,
                    args = {
                        supportText = {
                            name = L["SupportText"],
                            type = "description",
                            order = 1,
                        },
                    }
                }
            }
        }
    }
}
