local _, T = ...

local FeignDeathResistAlert = LibStub("AceAddon-3.0"):NewAddon(T, "FeignDeathResistAlert", "AceConsole-3.0", "AceEvent-3.0")
FeignDeathResistAlert.version = "1.0"
EXPOSED_STRING_EXPOSEWEAKNESS = "Perce-faille"

-- Localization
local L = LibStub("AceLocale-3.0"):GetLocale("FeignDeathResistAlert")

-- Default values
local defaults = {
    profile = {
        flashScreen = true,
        announceSay = true,
        announceGroup = true,
        messageFD = L["MessageDefaultFD"],
        messageEW = L["MessageDefaultEW"],
        sound = "scourge_horn"
    }
}

-- Determine if character is a hunter or not
function FeignDeathResistAlert:IsHunter()
    local _,_,classID = UnitClass("player")
    return classID == 3
end

-- Create and setup flash animation
-- Credit to unitscan addon
function FeignDeathResistAlert:SetupFlash()
    local flash = CreateFrame('Frame')
    self.flash = flash
    flash:Show()
    flash:SetAllPoints()
    flash:SetAlpha(0)
    flash:SetFrameStrata('FULLSCREEN_DIALOG')

    local texture = flash:CreateTexture()
    texture:SetBlendMode('ADD')
    texture:SetAllPoints()
    texture:SetTexture([[Interface\FullScreenTextures\LowHealth]])

    flash.animation = CreateFrame('Frame')
    flash.animation:Hide()
    local animLength = 2
    local loops = 3
    flash.animation:SetScript('OnUpdate', function(self)
        local t = GetTime() - self.t0
        if t <= .5 then
            flash:SetAlpha(t * 2)
        elseif t <= 1 then
            flash:SetAlpha(1)
        elseif t <= 1.5 then
            flash:SetAlpha(1 - (t - 1) * 2)
        else
            flash:SetAlpha(0)
            self.loops = self.loops - 1
            if self.loops == 0 then
                self.t0 = nil
                self:Hide()
            else
                self.t0 = GetTime()
            end
        end
    end)

    function flash.animation:Play()
        if self.t0 then
            self.loops = 4
        else
            self.t0 = GetTime()
            self.loops = 3
        end
        self:Show()
    end

    return flash.animation
end

-- Initalize addon event handler
function FeignDeathResistAlert:OnInitialize()
    -- Don't load if player is not a hunter
    if self:IsHunter() == false then
        return
    end

    print("|cff00ccffFeign Death Resist/Expose Weakness Alert loaded")

    local AceConfig = LibStub("AceConfig-3.0")
    local AceConfigDialog = LibStub("AceConfigDialog-3.0")

    self.db = LibStub("AceDB-3.0"):New("FeignDeathResistAlertDB", defaults, true)

    -- Register options table
    AceConfig:RegisterOptionsTable("FeignDeathResistAlert", FeignDeathResistAlert.options)
    LibStub("AceConfigRegistry-3.0"):RegisterOptionsTable("FeignDeathResistAlert", FeignDeathResistAlert.options)

    -- Profile options
    FeignDeathResistAlert.options.args.Profiles = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db)
    FeignDeathResistAlert.options.args.Profiles.order = -1

    -- Add to Blizz Interface Options
    local optionsFrames = {}
    optionsFrames.FeignDeathResistAlert = AceConfigDialog:AddToBlizOptions("FeignDeathResistAlert", L["FeignDeathResistAlert"], nil, "Options")
    optionsFrames.Profiles = AceConfigDialog:AddToBlizOptions("FeignDeathResistAlert", L["Profiles"], L["FeignDeathResistAlert"], "Profiles")

    -- Register slash commands
    self:RegisterChatCommand("feigndeathresistalert", "OpenOptions")

    -- Sound file definitions
    self.sounds = {
        ["scourge_horn"] = [[Interface\AddOns\FeignDeathResistAlert\sounds\scourge_horn.ogg]],
        ["grunt_horn"] = [[Interface\AddOns\FeignDeathResistAlert\sounds\gruntling_horn_bb.ogg]],
        ["war_drums"] = [[Interface\AddOns\FeignDeathResistAlert\sounds\Event_wardrum_ogre.ogg]],
    }

    -- Flash animation setup
    self.flash = self:SetupFlash()

    -- Alert throttling (seconds)
    self.throttleTime = 5
    self.throttledExposeWeakness = false
    self.throttled = false
end

-- Addon enable handler
function FeignDeathResistAlert:OnEnable()

    -- Don't load if player is not a hunter
    if self:IsHunter() == false then
        return
    end

    self:RegisterEvent("UI_ERROR_MESSAGE")
    self:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")

    print("|cff00ccffFeign Death Resist/Expose Weakness Alert enable")
end

-- Addon disable handler
function FeignDeathResistAlert:OnDisable()
    self:UnregisterEvent("UI_ERROR_MESSAGE")
    self:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
end

-- Open the Blizz Interface Options
function FeignDeathResistAlert:OpenOptions()
    InterfaceOptionsFrame_OpenToCategory("FeignDeathResistAlert")
    InterfaceOptionsFrame_OpenToCategory("FeignDeathResistAlert")
end

function FeignDeathResistAlert:DisplayCombatLog(event, ...)
    local timestamp, subevent, _, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags = ...
    local spellId, spellName, spellSchool
    local amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing, isOffHand

    if subevent == "SPELL_AURA_APPLIED" then
        spellId, spellName, spellSchool, amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing, isOffHand = select(12, ...)

        if spellName == "Perce-faille" then
            -- Check for throttling
            if self.throttledExposeWeakness == true then
                return
            end

            self.throttledExposeWeakness = true

            -- Play sound
            if self.db.profile.sound ~= "none" then
                PlaySoundFile(self.sounds[self.db.profile.sound], 'Master')
            end
            local messageEW = self:FormattedMessage(self.db.profile.messageEW)
            print("|cffDA70D6" .. messageEW)

--            -- Announcement in /say
--            if self.db.profile.announceSay then
--                SendChatMessage(messageEW, "SAY")
--            end
--
--            -- Announcement in party or raid
--            if self.db.profile.announceGroup then
--                if UnitInRaid("player") then
--                    SendChatMessage(messageEW, "RAID")
--                else
--                    if UnitInParty("player") then
--                        SendChatMessage(messageEW, "PARTY")
--                    end
--                end
--            end

--            -- Flash screen
--            if self.db.profile.flashScreen == true then
--                self.flash:Play()
--            end

            -- Reset throttle
            C_Timer.After(self.throttleTime, function()
                FeignDeathResistAlert.throttledExposeWeakness = false
            end)
        end
    end
--    if subevent == "SPELL_AURA_APPLIED" then
--        spellId, spellName, spellSchool, amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing, isOffHand = select(12, ...)
--        print(string.format("%s %s", subevent, spellName))
--    elseif subevent == "SPELL_DAMAGE" then
--        spellId, spellName, spellSchool, amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing, isOffHand = select(12, ...)
--        print(string.format("%s %s", subevent, spellName))
--    end

--    "SPELL_AURA_APPLIED"


--    local action = spellId and GetSpellLink(spellId) or MELEE
--    print(MSG_CRITICAL_HIT:format(action, destName, amount))

--    if critical and sourceGUID == playerGUID then
--        local action = spellId and GetSpellLink(spellId) or MELEE
--        print(MSG_CRITICAL_HIT:format(action, destName, amount))
--    end
end

-- Some event handler
function FeignDeathResistAlert:COMBAT_LOG_EVENT_UNFILTERED(_, event)
    self:DisplayCombatLog(event, CombatLogGetCurrentEventInfo())
end

function FeignDeathResistAlert:UI_ERROR_MESSAGE(_, arg1)
--    ERR_FEIGN_DEATH_RESISTED
    if arg1 == ERR_FEIGN_DEATH_RESISTED then
        -- Check for throttling
        if self.throttled == true then
            return
        end

        self.throttled = true

        -- Play sound
        if self.db.profile.sound ~= "none" then
            PlaySoundFile(self.sounds[self.db.profile.sound], 'Master')
        end

--        -- Flash screen
--        if self.db.profile.flashScreen == true then
--            self.flash:Play()
--        end

        local messageFD = self:FormattedMessage(self.db.profile.messageFD)

        print("|cffDA70D6" .. messageFD)
        -- Announcement in /say
--        if self.db.profile.announceSay then
--            SendChatMessage(messageFD, "SAY")
--        end
--
--        -- Announcement in party or raid
--        if self.db.profile.announceGroup then
--            if UnitInRaid("player") then
--                SendChatMessage(messageFD, "RAID")
--            else
--                if UnitInParty("player") then
--                    SendChatMessage(messageFD, "PARTY")
--                end
--            end
--        end

        -- Reset throttle
        C_Timer.After(self.throttleTime, function()
            FeignDeathResistAlert.throttled = false
        end)

    end
end

-- Format the announcement message
function FeignDeathResistAlert:FormattedMessage(message)
    -- Replace $p with player's name
    local message = string.gsub(message, "$p", UnitName("player"))
    -- Replace $t with target's name
    return string.gsub(message, "$t", UnitName("target"))
end
